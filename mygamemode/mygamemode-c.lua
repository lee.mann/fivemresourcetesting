local spawnPos = vector3(-275.522, 6635.835, 7.425)

-- Main Block
AddEventHandler("onClientGameTypeStart", 
    function()
        exports.spawnmanager:setAutoSpawnCallback(
            function() -- This function triggers when you die / first log in
                exports.spawnmanager:spawnPlayer({
                    x = spawnPos.x,
                    y = spawnPos.y,
                    z = spawnPos.z,
                    model = 'a_m_m_skater_01'
                    }, 
                    function()
                        TriggerEvent('chat:addMessage', {
                            args = { 'Welcome to the party!~' }
                        })
                    end)
            end)

        exports.spawnmanager:setAutoSpawn(true)
        exports.spawnmanager:forceRespawn()
    end
)