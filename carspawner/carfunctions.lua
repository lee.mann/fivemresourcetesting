function spawnCar(car)
    local car = GetHashKey(car)

    RequestModel(car)
    while not HasModelLoaded(car) do
        RequestModel(car)
        Citizen.Wait(0)
    end

    local x,y,z = table.unpack(GetEntityCoords(GetPlayerPed(-1), false))
    local vehicle = CreateVehicle(car, x + 3, y + 3, z + 1, 0.0, true, false)

    SetEntityAsMissionEntity(vehicle, true, true)
end

function alert(msg)
    SetTextComponentFormat("string")
    AddTextComponentString(msg)
    DisplayHelpTextFromStringLabel(0, 0, 1, -1)
end

function notify(msg)
    SetNotificationTextEntry("string")
    AddTextComponentString(msg)
    DrawNotification(true, false)
end