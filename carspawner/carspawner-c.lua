--[[RegisterCommand('car', 
    function(source, args)
        local vehicleName = args[1] or 'adder'

        if not IsModelInCdimage(vehicleName) or not IsModelAVehicle(vehicleName) then
            TriggerEvent('chat:addMessage', {
                args = { vehicleName .. ' is not a valid vehicle.' }
            })

            return
        end

        RequestModel(vehicleName)

        while not HasModelLoaded(vehicleName) do
            Wait(500)
        end

        local playerPed = PlayerPedId()
        local pos = GetEntityCoords(playerPed)

        local vehicle = CreateVehicle(vehicleName, pos.x, pos.y, pos.z, GetEntityHeading(playerPed), true, false)

        SetPedIntoVehicle(playerPed, vehicle, -1)

        SetEntityAsNoLongerNeeded(vehicle)

        SetModelAsNoLongerNeeded(vehicleName)

        TriggerEvent('chat:addMessage', {
            args = { vehicleName .. ' spawned. Enjoy.' }
        })
    end,
    false) --]]

local cars = {"adder", "comet", "akuma", "cheetah"}
RegisterCommand("car", function()
        local car = (cars[math.random(#cars)])
        spawnCar(car)
        notify("~p~Spawned Car: ~h~~g~" .. car)
    end
)