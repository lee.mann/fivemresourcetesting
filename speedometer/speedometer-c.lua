function text(content)
    SetTextFont(1)
    SetTextProportional(0)
    SetTextScale(1.5, 1.5)
    SetTextEntry("STRING")
    --[[
        For whatever reason, the below text function will not add the text component if
            the content passed in is, say, 0. I'm assuming because it treats a 0 int as
            a null? When you append the "MPH" it shows all the time.
    --]]
    AddTextComponentString(content .. " MPH")
    DrawText(0.85, 0.7)
end

Citizen.CreateThread(function()
        while true do
            Citizen.Wait(2)
            -- KPH factor = 3.6
            -- MPH factor = 2.2369
            local speed = GetEntitySpeed(GetVehiclePedIsIn(GetPlayerPed(-1, false))) * 2.2369

            if(IsPedInAnyVehicle(GetPlayerPed(-1, false))) then
                text(math.floor(speed))
            end
        end
    end
)