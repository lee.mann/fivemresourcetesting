RegisterCommand("clear", function()
        RemoveAllPedWeapons(GetPlayerPed(-1), true)
        notify("~r~Cleared All Weapons")
    end
)

RegisterCommand("die", function()
        SetEntityHealth(PlayerPedId(), 0)
        notify("~r~You Died.")
    end
)

Citizen.CreateThread(function()
        local h_key = 74
        local x_key = 73

        while true do
            Citizen.Wait(1)

            if IsControlJustReleased(1, h_key) then
                giveWeapon("weapon_SniperRifle")

                giveWeapon("weapon_pumpshotgun")
                weaponComponent("weapon_pumpshotgun", "COMPONENT_AT_AR_FLSH")
                weaponComponent("weapon_pumpshotgun", "COMPONENT_AT_SR_SUPP")

                giveWeapon("weapon_Knife")
                alert("~b~Given Weapons with ~INPUT_VEH_HEADLIGHT~")
            end

        end
    end
)