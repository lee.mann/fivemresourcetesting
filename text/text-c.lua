local color = {
    r = 255,
    g = 255,
    b = 255,
    a = 255
}

Citizen.CreateThread(function()
        while true do
            Citizen.Wait(5)

            -- Text Display
            SetTextFont(4) -- 0 -> 4
            SetTextScale(0.4, 0.4)
            SetTextColour(color.r, color.b, color.g, color.a)
            SetTextEntry("STRING")
            AddTextComponentString("This is the text to display")
            DrawText(0.45, 0.97)

            -- Rectangle Display
            --DrawRect(150, 100, 3.2, 0.05, 66, 134, 244, 245) -- ?, ?, x, y, r, g, b, a
        end
    end
)